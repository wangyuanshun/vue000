import Vue from 'vue'

//vvf
import { Header } from 'mint-ui'

Vue.component(Header.name,Header)

import './lib/mui/css/mui.min.css'
import './lib/mui/css/icons-extra.css'

import VueRouter from 'vue-router'

Vue.use(VueRouter)

import router from './router.js'


import { Swipe, SwipeItem,Button} from 'mint-ui';
Vue.component(Button.name,Button)
Vue.component(Swipe.name, Swipe);
Vue.component(SwipeItem.name, SwipeItem);

import app from './App.vue'

import VueResource from 'vue-resource'
Vue.use(VueResource)

//设置请求的根路径
Vue.http.options.root='http://www.liulongbin.top:3005/'

//导入时间moment插件
import moment from 'moment'

//定义全局的过滤器
Vue.filter('dateFormat',function(dateStr,pattern='YYYY-MM-DD HH:mm:ss'){
    return moment(dateStr).format(pattern)
})



var vm =new Vue({
    el:'#app',
    render:c=>c(app),
    router
    
})